var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.basketmath2p = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var blocProposition,aPanneau,aFilet,aEtiquette,panneauClique,aExpression,aResultat,aSoluce,soluce,resEval;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        totalEssai:1,
        tempsExo:0,
        tempsQuestion:0,
        cible:[1,2],
        typeMultiple:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt     :   "basketmath2p/textes/basketmath2p_fr.json",
    illustr :   "basketmath2p/images/basketmath.jpg",
    fond    :   "basketmath2p/images/fond.png",
    panneau :   "basketmath2p/images/panneau.png",
    ballon  :   "basketmath2p/images/ballon.png",
    filet   :   "basketmath2p/images/filet.png",
    rature  :   "basketmath2p/images/rature.png"
    // illustration : "template/images/illustration.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {

    aDonnee = creerDonnee(exo.options.typeMultiple,10);

    // retourne un tableau à trois dimension : relation, nombre A , nombre B
    function creerDonnee(type,nbreQuestion){
        var i,nombre,alea, aTemp, aNcourant;
        var aNombre = [];
        // double des nombres < 10 
        // et moitié des nombres pairs < 20
        if (type === 0 ){
            // trois doubles justes
            aTemp=[];
            for(i=0; i<3 ; i++ ){
                nombre = 2 + Math.floor(Math.random()*(9-2+1));
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["double",nombre,2*nombre]);
                } else {
                    i--;
                }
                
            }
            // trois doubles faux
            for(i=0; i<3 ; i++ ){
                nombre = 2 + Math.floor(Math.random()*(9-2+1));
                alea = 1 + Math.floor(Math.random()*(3-1+1));
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["double",nombre,2*nombre+alea]);
                } else {
                    i--;
                }
            }
            aTemp=[];
            // deux moitié justes
            for(i=0; i<2 ; i++ ){
                nombre = 4 + Math.floor(Math.random()*(18-4+1));
                if (nombre%2 === 0 && aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["moitie",nombre,Math.floor(nombre/2)]);
                } else {
                    i--;
                }
            }
            // deux moitié fausses
            for(i=0; i<2 ; i++ ){
                nombre = 4 + Math.floor(Math.random()*(18-4+1));
                if (nombre%2 == 1 && aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["moitie",nombre,Math.floor((nombre-1)/2)]);
                } else {
                    i--;
                }
            }
        }
        // double et moitié des nombres d'usage courant
        else if (type == 1 ){
            // trois doubles justes
            aNcourant = [5,6,7,8,9,10,12,15,20,24,25,30,50];
            aTemp=[];
            for(i=0; i<3 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["double",nombre,2*nombre]);
                } else {
                    i--;
                }
            }
            // trois doubles faux
            for(i=0; i<3 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                alea = 1 + Math.floor(Math.random()*(3-1+1));
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["double",nombre,2*nombre+alea]);
                } else {
                    i--;
                }
            }
            aTemp=[];
            aNcourant = [6,8,10,12,14,16,20,24,30,50];
            // un moitié justes
            for(i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["moitie",nombre,Math.floor(nombre/2)]);
                } else {
                    i--;
                }
            }
            // deux moitié fausses
            for(i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["moitie",nombre,Math.floor(nombre/2)-1]);
                } else {
                    i--;
                }
            }
        }
        //double moitié triple quart des nombres d'usage courant
        else if (type == 2 ){
            // un double juste
            aNcourant = [5,6,7,8,9,10,12,15,20,24,25,30,50];
            aTemp=[];
            util.shuffleArray(aNcourant);
            nombre = aNcourant[0];
            if (aTemp.indexOf(nombre)<0){
                aTemp.push(nombre);
                aNombre.push(["double",nombre,2*nombre]);
            }
            aNcourant = [6,8,10,12,14,16,20,24,30,50];
            // une moitié fausse
            for(i=0; i<1 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["moitie",nombre,Math.floor(nombre/2)-1]);
                } else {
                    i--;
                }
            }
            // deux triples justes
            aNcourant = [5,10,15,20,25,30,40,50];
            for( i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["triple",nombre,3*nombre]);
                } else {
                    i--;
                }
            }
            // deux triples faux
            for( i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["triple",nombre,3*nombre+1]);
                } else {
                    i--;
                }
            }
            aNcourant = [12,16,20,24,36,40,60,100];
            // deux quarts justes
            for( i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["quart",nombre,Math.floor(nombre/4)]);
                } else {
                    i--;
                }
            }
            // deux quarts faux
            for( i=0; i<2 ; i++ ){
                util.shuffleArray(aNcourant);
                nombre = aNcourant[0];
                if (aTemp.indexOf(nombre)<0){
                    aTemp.push(nombre);
                    aNombre.push(["quart",nombre,Math.floor(nombre/4)-1]);
                } else {
                    i--;
                }
            }
        }
        util.shuffleArray(aNombre);
        return aNombre;
    }

    console.log(aDonnee);
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustr");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    var vH,vV,vW,dX,dY,t,rot,g;
    //
    exo.btnValider.hide();
    exo.keyboard.config({
        numeric:"disabled",
        arrow:"disabled",
        large:"disabled"
    });
    exo.blocInfo.css({left:(735-exo.blocInfo.width())/2,top:330});
    
    // le fond
    var fond = disp.createImageSprite(exo,"fond");
    exo.blocAnimation.append(fond);
    
    var listeExpression = ["vrai","faux"];
    // les panneaux
    var aPosPanneau = [83,493];
    aPanneau=[];
    var i;
    for ( i=0; i<2 ; i++ ){
        aPanneau[i] = disp.createImageSprite(exo,"panneau");
        aPanneau[i].addClass("clickable");
        aPanneau[i].data({index:i});
        aPanneau[i].data({valeur:listeExpression[i]});
        aPanneau[i].css({left:aPosPanneau[i], top:50});
        exo.blocAnimation.append(aPanneau[i]);
    }

    // les filets
    var aPosFilet = [120.77,530.77];
    aFilet=[];
    for ( i=0; i<2 ; i++ ){
        aFilet[i] = disp.createImageSprite(exo,"filet");
        aFilet[i].addClass("clickable");
        aFilet[i].data({index:i});
        aFilet[i].data({valeur:listeExpression[i]});
        aFilet[i].css({left:aPosFilet[i], top:130.5});
        exo.blocAnimation.append(aFilet[i]);
        //aFilet[i].on("mousedown.clc touchstart.clc",gererClickPanneau);
    }
    
    // les etiquettes sur les panneaux
    
    aEtiquette=[];
    for ( i=0; i<2 ; i++ ){
        aEtiquette[i] = disp.createTextLabel(listeExpression[i]).css({textTransform:"uppercase"});
        aEtiquette[i].addClass("clickable");
        aEtiquette[i].data({index:i});
        aEtiquette[i].data({valeur:listeExpression[i]});
        exo.blocAnimation.append(aEtiquette[i]);
        aEtiquette[i].position({my:"center top",at:"center top+20",of:aPanneau[i]});
        //aEtiquette[i].on("mousedown.clc touchstart.clc",gererClickPanneau);
    }
    
    // la proposition, la solution
    var proposition;
    var soluce = "faux";
    var relation = aDonnee[exo.indiceQuestion][0];
    var nA = aDonnee[exo.indiceQuestion][1];
    var nB = aDonnee[exo.indiceQuestion][2];
    if( relation == "double"){
        proposition = "Le double de " + nA + " c'est " + nB + ".";
        if( nB == 2 * nA )
            soluce = "vrai";
    }
    else if( relation == "moitie" ){
        proposition = nB + " c'est la moitié de " + nA + ".";
        if( nA == 2 * nB )
            soluce = "vrai";
    }
    else if( relation == "triple" ){
        proposition = "Le triple de " + nA + " c'est " + nB + ".";
        if( nB == 3 * nA )
            soluce = "vrai";
    }
    else if( relation == "quart" ){
        proposition = nB + " c'est le quart de " + nA + ".";
        if( nA == 4 * nB )
            soluce = "vrai";
    }
    blocProposition = disp.createTextLabel(proposition).css({fontSize:28,fontWeight:"bold"});
    exo.blocAnimation.append(blocProposition);
    blocProposition.css({top:280,left:-blocProposition.width()});

    // les messages propres à cet exercice
    if(soluce == "vrai"){
        exo.str.info.msgFauxQuestionSuivante = "Manqué !<br>Cette phrase est vraie.";
        exo.str.info.msgTempsDepasseFaux = "Temps dépassé !<br>Il fallait cliquer sur VRAI.";
    }
    else {
        exo.str.info.msgFauxQuestionSuivante = "Manqué !<br>Cette phrase est fausse.";
        exo.str.info.msgTempsDepasseFaux = "Temps dépassé !<br>Il fallait cliquer sur FAUX.";
    }
    
    // le ballon
    var ballon = disp.createImageSprite(exo,"ballon");
    var imgBallon = ballon.children("img");
    exo.blocAnimation.append(ballon);
    ballon.position({my:"center center", at:"center bottom-50", of:exo.blocAnimation});
    
    // gestion du click sur les panneaux
    repEleve="rien";
    $(".clickable").on("mousedown.clc touchstart.clc",gererClickPanneau);

    // on lance l'apparition du bloc proposition
    var posX = blocProposition.width()+(735-blocProposition.width())/2;
    blocProposition.transition({x:posX});
    
    
    /* ***************************** */
    function gererClickPanneau(e){
        e.preventDefault();
        if(exo.options.temps_question>0)
            exo.chrono.stop();
        $(".clickable").off("mousedown.clc touchstart.clc");
        
        panneauClique = $(this);
        repEleve = panneauClique.data().valeur;
        //resEval = "rien";
        if ( repEleve == soluce ) {
            resEval = "juste";
        } 
        else {
            
            resEval = "faux";
        }
        
        var animId;
        
        for ( var i=0; i<2 ; i++ ){
            aPanneau[i].off("mousedown.clc touchstart.clc");
        }
        vV = 800;
        rot = "-=10deg";
        t = 0;
        g = 880;
        dY = ballon.position().top;
        dX = ballon.position().left+ballon.width()/2;
        vW = 25;
        if (panneauClique.data().index === 0 && resEval == "juste" ) {
            vH = -150;
        }
        else if (panneauClique.data().index == 1 && resEval == "juste" ) {
            vH = 150;
        }
        else if (panneauClique.data().index === 0 && resEval == "faux" ) {
            vH = -200;
        }
        else if (panneauClique.data().index == 1 && resEval == "faux" ) {
            vH = 200;
        }
        shooter();
    }
    
    function shooter(dt) {
        animId = requestAnimationFrame(shooter);
        var posX,posY;
        //premiere partie de la trajectoire jusqu'au panier
        if ((ballon.position().top <= dY && g*t <= vV) || (ballon.position().top <= 80 && g*t > vV))
        {
            posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            posX = vH*t + dX;
            var largeur = 80-vW*t;
            ballon.css({left:posX-ballon.width()/2,top:posY,width:largeur,height:largeur});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        //deuxième partie redescente à la verticale
        else if(ballon.position().top<250 && ballon.position().top > 80 && g*t > vV ){
            //on fait passer les filets devant
            for ( var i=0; i<2 ; i++ ){
                aFilet[i].css({zIndex:1000});
            }
            posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            posX = -100*t + dX;
            ballon.css({top:posY});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        // arret de l'anim
        else if( ballon.position().top>=250 && g*t > vV){
            cancelAnimationFrame(animId);
            vV = 200;
            t = 0;
            g=440;
            vH *= 3/4;
            dY = ballon.position().top;
            dX = ballon.position().left;
            if (panneauClique.data().index === 0) {
                rebondirGauche();
            } else if (panneauClique.data().index == 2) {
                rebondirDroite();
            } if (panneauClique.data().index == 1) {
                rebondir();
            }
        }
    }
    
    function manquerShoot(dt) {
        vH=-200;
        animId = requestAnimationFrame(reussirShoot);
        var posX,posY;
        //premiere partie de la trajectoire jusqu'au panier
        if ( (ballon.position().top <= dY && g*t <= vV) || (ballon.position().top <= 80 && g*t > vV) )
        {
            posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            posX = vH*t + dX;
            ballon.css({left:posX-ballon.width()/2,top:posY,width:dW});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        //deuxième partie redescente à la verticale
        else if(ballon.position().top<250 && ballon.position().top > 80 && g*t > vV ){
            //on fait passer les filets devant
            for ( var i=0; i<2 ; i++ ){
                aFilet[i].css({zIndex:1000});
            }
            posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            posX = -100*t + dX;
            ballon.css({top:posY});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        // arret de l'anim
        else if( ballon.position().top>=250 && g*t > vV){
            cancelAnimationFrame(animId);
            vV = 200;
            t = 0;
            g=440;
            vH *= 3/4;
            dY = ballon.position().top;
            dX = ballon.position().left;
            if (panneauClique.data().index === 0) {
                rebondirGauche();
            } else if (panneauClique.data().index == 2) {
                rebondirDroite();
            } if (panneauClique.data().index == 1) {
                rebondir();
            }
        }
    }
    
    function rebondirGauche(dt){
        animId = requestAnimationFrame(rebondirGauche);
        if (ballon.position().top<=dY && ballon.position().left >-ballon.width() ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            ballon.css({left:posX,top:posY});
            imgBallon.css({rotate:"-=1deg"});
            t+=1/31;
        } else if (ballon.position().left >-ballon.width() ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            vH *= 3/4;
            dX = ballon.position().left;
            ballon.css({top:dY});
            rebondirGauche();
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
    
    function rebondirDroite(dt){
        animId = requestAnimationFrame(rebondirDroite);
        if (ballon.position().top<=dY && ballon.position().left < 735 + ballon.width() ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            ballon.css({left:posX,top:posY});
            imgBallon.css({rotate:"+=1deg"});
            t+=1/31;
        } else if (ballon.position().left < 735+ballon.width() ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            vH *= 3/4;
            dX = ballon.position().left;
            ballon.css({top:dY});
            rebondirDroite();
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
    
    function rebondir(dt){
        animId = requestAnimationFrame(rebondir);

        if (ballon.position().top <= dY ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            ballon.css({top:posY});
            t+=1/31;
        } else if ( vV > 150 ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            dX = ballon.position().left;
            ballon.css({top:dY});
            rebondir();
            
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
   return resEval;
};

// Correction (peut rester vide)
exo.corriger = function() {
    var q = exo.indiceQuestion;
    var relation = aDonnee[q][0];
    var nA = aDonnee[q][1];
    var nB = aDonnee[q][2];
    var sCorrection = "";
    // l'eleve a répondu vrai alors que la phrase est fausse
    // on met en evidence ce qui est faux dans la proposition
    if (repEleve == "vrai" ) {
        if ( relation == "double" ) {
            sCorrection = 2*nA;
        }
        else if ( relation == "moitie" ) {
            sCorrection = nB*2;
        }
        else if ( relation == "triple" ) {
            sCorrection = nA*3;
        }
        else if ( relation == "quart" ) {
            sCorrection = nB*4;
        }
        var labelCorrection = disp.createTextLabel(sCorrection);
        labelCorrection.css({fontSize:28,fontWeight:"bold",color:"#ff0000"});
        exo.blocAnimation.append(labelCorrection);
        var posX = blocProposition.position().left + blocProposition.width() - labelCorrection.width();
        var posY = blocProposition.position().top - labelCorrection.height();
        labelCorrection.css({left:posX,top:posY});
        var rature = disp.createImageSprite(exo,"rature");
        exo.blocAnimation.append(rature);
        posX = blocProposition.position().left + blocProposition.width() - rature.width()+7;
        posY = blocProposition.position().top;
        rature.css({left:posX,top:posY});
        
    }
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:36,
        taille:2,
        nom:"tempsQuestion",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typeMultiple",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[0,1,2]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));